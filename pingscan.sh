#!/bin/bash

##################################################
#
# Name:          pingscan.sh
# Author:        Jefferson Kirkland 
# Description:   A scanner that takes a network
#    as input and scans each IP on the network to
#    see if it is alive.
#
# Usage:   pingscan.sh <network>
#
#    <network> = The first 3 octets of an 
#                IPv4 address (ie: 192.168.1)
#
##################################################


function usage() {
  echo "Usage:   pingscan.sh <network>"
  echo "<network> = The first 3 octets of an IPv4 address (ie: 192.168.1)"
}

function scan() {
  for i in `seq 1 254`
  do
    ip="${net}.${i}"
    echo "Checking ${ip}......"
    return=`ping -c 1 -t 4 $ip`
    while read l
    do
      echo "l is $l"
      if [[ $l =~ "64 bytes from" ]]
      then
        echo "IP ${ip} active.  Adding to array..."
        ips_array[$[${#ips_array[@]}+1]]=${ip}
      fi
    done < <(echo $return)
  done

  echo ""
  echo ""
  echo "For network ${net} we have found the following IPs active:"
  for i in `seq 0 ${#ips_array[@]}`
  do
    echo "${ips_array[$i]}"
  done
}

net="$1"

if [[ $1 == "" ]]
then
   usage
else
   scan
fi

