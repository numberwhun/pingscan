Pingscan
--------

The purpuse of pingscan is to take a network (which is typically the first 
three octets of an ip address) as its only option to the script, and ping
each IP address in that network.

The usage of the script is as follows:

	pingscan.sh <network>


    <network> = The first 3 octets of an 
                IPv4 address (ie: 192.168.1)

The script will return the IP's that it has found at the end of its run.


